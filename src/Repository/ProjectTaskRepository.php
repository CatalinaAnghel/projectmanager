<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\ProjectTask;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProjectTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectTask[]    findAll()
 * @method ProjectTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectTask::class);
    }

    public function insert(ProjectTask $task)
    {
        // Get the connection
        $conn = $this->getEntityManager()->getConnection();
        $projectId = $task->getProject()->getId();
        $priority = $task->getPriority();
        $description = $task->getDescription();
        $difficulty = $task->getDifficulty();
        $estimatedTime = $task->getEstimatedTime();
        $status = $task->getStatus();
        // The raw sql
        $sql = "INSERT INTO `project_task`(`project_id`,`priority`,`description`,`difficulty`,`estimated_time`,`status`) VALUES (" . $projectId . "," . "\"" . $priority . "\"" . "," . "\"" . $description . "\"" . "," . "\"" . $difficulty . "\"" . "," . "\"" . $estimatedTime . "\"" . "," . "\"" . $status . "\"" . ")";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
    }

    // de schimbat
    public function update(ProjectTask $task)
    {
        // Get the connection
        $conn = $this->getEntityManager()
            ->getConnection();

        $id = $task->getId();
        $projectId = $task->getProject()->getId();
        $priority = $task->getPriority();
        $description = $task->getDescription();
        $difficulty = $task->getDifficulty();
        $estimatedTime = $task->getEstimatedTime();
        $status = $task->getStatus();
        // The raw sql
        $sql = "UPDATE `project_task` SET `project_id`=" . $projectId . ",`priority`=" . "\"" . $priority . "\"" . ",`status`=" . "\"" . $status . "\"" . ",`description`=" . "\"" . $description . "\"" . ",`estimated_time`=" . $estimatedTime . ",`difficulty`=" . "\"" . $difficulty . "\"" . " WHERE `id`=" . $id;
        $stmt = $conn->prepare($sql);

        $stmt->execute();
    }

    public function findById(int $id, ProjectRepository $repository, UserRepository $userRepository): ?ProjectTask
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT * FROM `project_task` WHERE `id`=" . $id;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if ($result == null) {
            return null;
        } else {
            $foundProject = $repository->findById($userRepository, $result['project_id']);
            $task = new ProjectTask();
            $task->setId($result['id']);
            $task->setPriority($result['priority']);
            $task->setDifficulty($result['difficulty']);
            $task->setDescription($result['description']);
            $task->setEstimatedTime($result['estimated_time']);
            $task->setProject($foundProject);
            $task->setStatus($result['status']);

            return $task;
        }
    }

    public function findAllTasksOfCurrentUser(User $user, ProjectRepository $repo, UserRepository $uRepo): ?array
    {
        $conn = $this->getEntityManager()->getConnection();
        // The raw sql
        $sql = "SELECT * FROM ( `project_task` INNER JOIN `project` ON `project_task`.`project_id`=`project`.`id` AND `project`.`user_id`=" . $user->getId() . ")" ;
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $results = $stmt->fetchAll();
        $projects = [];
        if ($results == false) {
            return null;
        } else {
            foreach ($results as $result) {
                $tasks[$result['id']] = new ProjectTask();
                $tasks[$result['id']]->setId($result['id']);
                $project = $repo->findById($uRepo, $result['project_id']);
                $tasks[$result['id']]->setProject($project);
                $tasks[$result['id']]->setPriority($result['priority']);
                $tasks[$result['id']]->setStatus($result['status']);
                $tasks[$result['id']]->setDescription($result['description']);
                $tasks[$result['id']]->setLink($result['estimated_time']);
                $tasks[$result['id']]->setDifficulty($result['difficulty']);
            }
            return $tasks;
        }
    }

    public function delete(ProjectTask $task)
    {
        // Get the connection
        $conn = $this->getEntityManager()
            ->getConnection();

        $id = $task->getId();
        // The raw sql
        $sql = "DELETE FROM `project_task`WHERE `id`=" . $id;
        $stmt = $conn->prepare($sql);

        $stmt->execute();
    }

    // /**
    //  * @return ProjectTask[] Returns an array of ProjectTask objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjectTask
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
