<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneByUsername($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :user')
            ->setParameter('user', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    public function findOneByIdRawSQL($value): ?User
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "SELECT * FROM `user` WHERE `id`=" . $value;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $user = new User();
        $user->setUsername($result['username']);
        $user->setEmail($result['email']);
        $user->setPassword($result['password']);
        $user->setId($result['id']);
        return $user;
    }

    /**
     * @return User[]|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findAllUsers(): ?array{
        $conn = $this->getEntityManager()
            ->getConnection();
        // The raw sql
        $sql = "SELECT * FROM `user`";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $results = $stmt->fetchAll();
        $users = [];
        foreach ($results as $result){
            $users[$result["id"]] = new User();
            $users[$result["id"]]->setId($result["id"]);
            $users[$result["id"]]->setUsername($result["username"]);
        }

        return $users;
    }
    /*public function findOneByUsername($value): ?User
    {
        $rsm = new ResultSetMapping();
        // build rsm here
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createNativeQuery('SELECT * FROM user u WHERE u.id = id', $rsm);
        $query->setParameter(':id', $value);

        $user = $query->getResult();
        var_dump($user);
        return $user;
    }*/
}
