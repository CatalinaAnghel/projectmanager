<?php

namespace App\Repository;

use App\Entity\ProjectCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProjectCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectCode[]    findAll()
 * @method ProjectCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectCode::class);
    }

    // /**
    //  * @return ProjectCode[] Returns an array of ProjectCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjectCode
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
