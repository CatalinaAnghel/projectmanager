<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Driver\PDOConnection;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function insert(Project $project)
    {
        // Get the connection
        $conn = $this->getEntityManager()
            ->getConnection();

        $userId = $project->getUser()->getId();
        $dueTime = $project->getDueTime()->format('Y-m-d H:i:s');
        $status = $project->getStatus();
        $description = $project->getDescription();
        $link = $project->getLink();
        $workedHours = $project->getWorkedHours();
        $difficulty = $project->getDifficulty();
        $accessCode = $project->getAccessCode();
        $name = $project->getName();
        // The raw sql
        $sql = "INSERT INTO `project`(`user_id`,`due_time`,`status`,`description`,`link`,`worked_hours`,`difficulty`,`access_code`,`name`) VALUES (" . $userId . "," . "\"" . $dueTime . "\"" . "," . "\"" . $status . "\"" . "," . "\"" . $description . "\"" . "," . "\"" . $link . "\"" . "," . $workedHours . "," . "\"" . $difficulty . "\"" . "," . "\"" . $accessCode . "\"" . "," . "\"" . $name . "\"" . ")";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
    }

    public function insertUserAndProject(Project $project, User $user)
    {
        // Get the connection
        $conn = $this->getEntityManager()->getConnection();

        $userId = $user->getId();
        $projectId = $project->getId();
        // The raw sql
        $sql = "INSERT INTO `user_project`(`user_id`,`project_id`) VALUES (" . $userId . "," . $projectId . ")";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
    }

    public function delete(Project $project)
    {
        // Get the connection
        $conn = $this->getEntityManager()
            ->getConnection();

        $id = $project->getId();
        // The raw sql
        $sql = "DELETE FROM `project`WHERE `id`=" . $id;
        $stmt = $conn->prepare($sql);

        $stmt->execute();
    }

    public function update(Project $project)
    {
        // Get the connection
        $conn = $this->getEntityManager()
            ->getConnection();

        $id = $project->getId();
        $userId = $project->getUser()->getId();
        $date = $project->getDueTime()->format('Y-m-d');
        $status = $project->getStatus();
        $description = $project->getDescription();
        $link = $project->getLink();
        $workedHours = $project->getWorkedHours();
        $difficulty = $project->getDifficulty();
        $accessCode = $project->getAccessCode();
        $name = $project->getName();
        // The raw sql
        $sql = "UPDATE `project` SET `id`=" . $id . ",`user_id`=" . $userId . ",`due_time`=" . "\"" . $date . "\"" . ",`status`=" . "\"" . $status . "\"" . ",`description`=" . "\"" . $description . "\"" . ",`link`=" . "\"" . $link . "\"" . ",`worked_hours`=" . $workedHours . ",`difficulty`=" . "\"" . $difficulty . "\"" . ",`access_code`=" . "\"" . $accessCode . "\"" . ",`name`= " . "\"" . $name . "\"" . " WHERE id=" . $id;
        $stmt = $conn->prepare($sql);

        $stmt->execute();
    }

    public function findOneByUserAndName(User $user, String $name): ?Project
    {
        $conn = $this->getEntityManager()
            ->getConnection();

        $userId = $user->getId();
        // The raw sql
        $sql = "SELECT * FROM `project` WHERE `user_id`=" . $userId . " AND `name`=" . "\"" . $name . "\"";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $result = $stmt->fetch();
        if ($result == false) {
            return null;
        } else {
            $project = new Project();
            $project->setId($result['id']);
            $project->setUser($user);
            $s = $result['due_time'];
            $date = date_create($s);
            $project->setDueTime($date);
            $project->setStatus($result['status']);
            $project->setDescription($result['description']);
            $project->setDifficulty($result['difficulty']);
            $project->setAccessCode($result['access_code']);
            $project->setLink($result['link']);
            $project->setName($result['name']);
            $project->setWorkedHours($result['worked_hours']);
            return $project;
        }
    }

    public function findOneByAccessCode(UserRepository $userRepository, String $accessCode): ?Project
    {
        $conn = $this->getEntityManager()->getConnection();
        // The raw sql
        $sql = "SELECT * FROM `project` WHERE `access_code`=" . "\"". $accessCode . "\"";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $result = $stmt->fetch();
        if ($result == false) {
            return null;
        } else {
            $project = new Project();
            $project->setId($result['id']);
            $user = $userRepository->findOneByIdRawSQL($result['user_id']);
            $project->setUser($user);
            $s = $result['due_time'];
            $date = date_create($s);
            $project->setDueTime($date);
            $project->setStatus($result['status']);
            $project->setDescription($result['description']);
            $project->setDifficulty($result['difficulty']);
            $project->setAccessCode($result['access_code']);
            $project->setLink($result['link']);
            $project->setName($result['name']);
            $project->setWorkedHours($result['worked_hours']);
            return $project;
        }
    }

    public function findById(UserRepository $userRepository, int $id): ?Project
    {
        $conn = $this->getEntityManager()->getConnection();
        // The raw sql
        $sql = "SELECT * FROM `project` WHERE `id`=" . $id;
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $result = $stmt->fetch();
        if ($result == false) {
            return null;
        } else {
            $project = new Project();
            $project->setId($result['id']);
            $user = $userRepository->findOneByIdRawSQL($result['user_id']);
            $project->setUser($user);
            $s = $result['due_time'];
            $date = date_create($s);
            $project->setDueTime($date);
            $project->setStatus($result['status']);
            $project->setDescription($result['description']);
            $project->setDifficulty($result['difficulty']);
            $project->setAccessCode($result['access_code']);
            $project->setLink($result['link']);
            $project->setName($result['name']);
            $project->setWorkedHours($result['worked_hours']);
            return $project;
        }
    }

    /**
     * @return Project[]|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findAllProjects(array $users): ?array
    {
        $conn = $this->getEntityManager()
            ->getConnection();
        // The raw sql
        $sql = "SELECT * FROM `project`";
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $results = $stmt->fetchAll();
        $projects = [];
        if ($results == false) {
            return null;
        } else {
            foreach ($results as $result) {
                $projects[$result['id']] = new Project();
                $projects[$result['id']]->setId($result['id']);
                $user = $users[$result['user_id']];
                $s = $result['due_time'];
                $date = date_create($s);
                $projects[$result['id']]->setUser($user);
                $projects[$result['id']]->setDueTime($date);
                $projects[$result['id']]->setStatus($result['status']);
                $projects[$result['id']]->setDescription($result['description']);
                $projects[$result['id']]->setLink($result['link']);
                $projects[$result['id']]->setWorkedHours($result['worked_hours']);
                $projects[$result['id']]->setDifficulty($result['difficulty']);
                $projects[$result['id']]->setAccessCode($result['access_code']);
                $projects[$result['id']]->setName($result['name']);
            }
            return $projects;
        }
    }

    public function findProjectFromOtherUser(Project $project, User $user) : bool
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT `project_id`, `user_id` from `user_project` where `project_id`=". $project->getId() . " AND `user_id`=" . $user->getId();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if($result == false)
        {
            return false;
        }
        else{
            return true;
        }
    }

    // /**
    //  * @return Project[] Returns an array of Project objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Project
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
