<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190917180741 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project CHANGE due_time due_time DATE DEFAULT NULL, CHANGE worked_hours worked_hours DOUBLE PRECISION DEFAULT NULL, CHANGE difficulty difficulty VARCHAR(255) DEFAULT NULL, CHANGE access_code access_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE project_task CHANGE estimated_time estimated_time DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project CHANGE due_time due_time DATE DEFAULT \'NULL\', CHANGE worked_hours worked_hours DOUBLE PRECISION DEFAULT \'NULL\', CHANGE difficulty difficulty VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE access_code access_code VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE project_task CHANGE estimated_time estimated_time DOUBLE PRECISION DEFAULT \'NULL\'');
    }
}
