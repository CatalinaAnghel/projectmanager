<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190929083126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project ADD user_with_access_id INT DEFAULT NULL, CHANGE due_time due_time DATE DEFAULT NULL, CHANGE worked_hours worked_hours DOUBLE PRECISION DEFAULT NULL, CHANGE difficulty difficulty VARCHAR(255) DEFAULT NULL, CHANGE access_code access_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE905D0CFF FOREIGN KEY (user_with_access_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE905D0CFF ON project (user_with_access_id)');
        $this->addSql('ALTER TABLE project_task CHANGE estimated_time estimated_time DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP access_codes_to_other_projects, CHANGE salt salt VARCHAR(255) DEFAULT NULL, CHANGE last_login last_login DATETIME DEFAULT NULL, CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT NULL, CHANGE password_requested_at password_requested_at DATETIME DEFAULT NULL, CHANGE name name VARCHAR(255) DEFAULT NULL, CHANGE phone_number phone_number INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE905D0CFF');
        $this->addSql('DROP INDEX IDX_2FB3D0EE905D0CFF ON project');
        $this->addSql('ALTER TABLE project DROP user_with_access_id, CHANGE due_time due_time DATE DEFAULT \'NULL\', CHANGE worked_hours worked_hours DOUBLE PRECISION DEFAULT \'NULL\', CHANGE difficulty difficulty VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE access_code access_code VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE project_task CHANGE estimated_time estimated_time DOUBLE PRECISION DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user ADD access_codes_to_other_projects VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE salt salt VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE last_login last_login DATETIME DEFAULT \'NULL\', CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE password_requested_at password_requested_at DATETIME DEFAULT \'NULL\', CHANGE name name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE phone_number phone_number INT DEFAULT NULL');
    }
}
