<?php

namespace App\Form;

use App\Entity\ProjectTask;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;


class RemoveForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projects', ChoiceType::class, ['mapped' => false, 'multiple' => true,
                'expanded' =>true, 'label' => 'Project/Projects you want to delete:',
                'choices' => $options['data']['projects']] )
            ->add('remove', SubmitType::class, ['label' => 'Remove']);
    }

    /*public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectTask::class,
        ]);
    }*/
}
