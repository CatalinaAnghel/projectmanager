<?php

namespace App\Form;

use App\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;


class AddProjectForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Project name'])
            ->add('dueTime', DateType::class, ['label' => 'Due time'])
            ->add('status', ChoiceType::Class, ['choices' => [
                'Not started' => 'not started',
                'Started, but unfinished' => 'unfinished',
                'Finished' => 'finished'],])
            ->add('description', TextType::class)
            ->add('link', TextType::class)
            ->add('workedHours', IntegerType::class)
            ->add('difficulty', ChoiceType::class, ['choices' =>[
                'Easy' => 'easy',
                'Intermediate' => 'intermediate',
                'Hard' => 'hard'],
                ])
            ->add('add', SubmitType::class, ['label' => 'Add the project']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
