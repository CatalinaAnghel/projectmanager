<?php

namespace App\Form;

use App\Entity\ProjectTask;
use App\Entity\Project;
use App\Repository\ProjectRepository;
use App\Repository\ProjectTaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;


class ChangeTaskForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('task', ChoiceType::class, ['mapped' => false, 'label' => 'Pick a task',
            'choices' => $options['data']['tasks'], 'placeholder' => 'Pick a task', ])
            ->add('priority', ChoiceType::class, ['choices' =>[
                'Low' =>'low',
                'Medium' => 'medium',
                'High' => 'high',
                'Very high' => 'very high',
            ],])
            ->add('difficulty', ChoiceType::class, ['choices' =>[
                'Easy' =>'easy',
                'Intermediate' => 'intermediate',
                'Hard' => 'hard',
            ],])
            ->add('estimatedTime', IntegerType::class, ['constraints' => [new NotBlank(), new Positive()]])
            ->add('description', TextType::class)
            ->add('status', ChoiceType::class, ['choices' =>[
                'In progress' => 'In progress',
                'Canceled' => 'Canceled',
                'Done' => 'Done',
                'Reopened' => 'Reopened',
            ],])
            ->add('change', SubmitType::class, ['label' => 'Change task']);
    }

}