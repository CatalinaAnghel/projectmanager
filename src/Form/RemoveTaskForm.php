<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\ProjectTask;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;

class RemoveTaskForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('task', ChoiceType::class, [
                'mapped' =>false,
                'placeholder' => 'Select a task',
                'choices' => $options['data']['tasks'],
            ])
            ->add('remove', SubmitType::class, ['label' => 'Remove']);
        ;


    }

}
