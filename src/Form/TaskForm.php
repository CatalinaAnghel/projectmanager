<?php

namespace App\Form;

use App\Entity\ProjectTask;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;


class TaskForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projectName', ChoiceType::class, ['choices' => $options['data']['projects'], 'mapped' => false])
            ->add('priority', ChoiceType::class, ['choices' =>[
                'Low' =>'low',
                'Medium' => 'medium',
                'High' => 'high',
                'Very high' => 'very high',
            ],])
            ->add('difficulty', ChoiceType::class, ['choices' =>[
                'Easy' =>'easy',
                'Intermediate' => 'intermediate',
                'Hard' => 'hard',
            ],])
            ->add('estimatedTime', IntegerType::class, ['constraints' => [new NotBlank(),]])
            ->add('description', TextType::class)
            ->add('status', ChoiceType::class, ['choices' =>[
                'In progress' => 'In progress',
                'Canceled' => 'Canceled',
                'Done' => 'Done',
            ],])
            ->add('add', SubmitType::class, ['label' => 'Add task']);
    }

    /*public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectTask::class,
        ]);
    }*/
}
