<?php

namespace App\Form;

use App\Entity\ProjectTask;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Positive;


class ChangeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('project', ChoiceType::class, ['mapped' => false, 'label' => 'Project you want to change:',
                'choices' => $options['data']['projects']] )
            ->add('name', TextType::class, ['label' => 'New name of the project', 'empty_data' => '', 'required' => false,])
            ->add('dueTime', DateType::class, ['label' => 'New due time of the project'])
            ->add('status', ChoiceType::class, ['label' => 'New status of the project', 'choices' => [
                'Not started' => 'not started',
                'Started, but unfinished' => 'unfinished',
                'Finished' => 'finished'
            ]])
            ->add('accessCode', ChoiceType::class, ['label' => 'Do you want to generate a new access code?', 'choices' => [
                'Yes' => true,
                'No' => false,
            ]])
            ->add('description', TextType::class, ['empty_data' => '', 'required' => false,])
            ->add('link', TextType::class, ['empty_data' => '', 'required' => false,])
            ->add('workedHours', IntegerType::class, ['empty_data' => '', 'required' => false, 'constraints' => [new Positive()]])
            ->add('difficulty', ChoiceType::class, ['choices' =>[
                'Easy' => 'easy',
                'Intermediate' => 'intermediate',
                'Hard' => 'hard'],
            ])
            ->add('change', SubmitType::class, ['label' => 'Change']);
    }

    /*public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectTask::class,
        ]);
    }*/
}
