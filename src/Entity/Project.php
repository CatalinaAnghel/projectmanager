<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dueTime;

    /**
     * @ORM\Column(type="text")
     */
    private $status;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $workedHours;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $difficulty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accessCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectTask", mappedBy="project", orphanRemoval=true)
     */
    private $tasks;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="otherProjects")
     */
    private $usersWithAccess;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
        $this->usersWithAccess = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $value)
    {
        $this->id = $value;
    }

    public function getDueTime(): ?\DateTimeInterface
    {
        return $this->dueTime;
    }

    public function setDueTime(?\DateTimeInterface $dueTime): self
    {
        $this->dueTime = $dueTime;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getWorkedHours(): ?float
    {
        return $this->workedHours;
    }

    public function setWorkedHours(?float $workedHours): self
    {
        $this->workedHours = $workedHours;

        return $this;
    }

    public function getDifficulty(): ?string
    {
        return $this->difficulty;
    }

    public function setDifficulty(?string $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getAccessCode(): ?string
    {
        return $this->accessCode;
    }

    public function setAccessCode(?string $accessCode): self
    {
        $this->accessCode = $accessCode;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|projectTask[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(projectTask $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setProject($this);
        }

        return $this;
    }

    public function removeTask(projectTask $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getProject() === $this) {
                $task->setProject(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsersWithAccess(): Collection
    {
        return $this->usersWithAccess;
    }

    public function addUsersWithAccess(User $usersWithAccess): self
    {
        if (!$this->usersWithAccess->contains($usersWithAccess)) {
            $this->usersWithAccess[] = $usersWithAccess;
            $usersWithAccess->addOtherProject($this);
        }

        return $this;
    }

    public function removeUsersWithAccess(User $usersWithAccess): self
    {
        if ($this->usersWithAccess->contains($usersWithAccess)) {
            $this->usersWithAccess->removeElement($usersWithAccess);
            $usersWithAccess->removeOtherProject($this);
        }

        return $this;
    }

    public function __toString() {
        return $this->name;
    }
}
