<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Project;
use FOS\UserBundle\Model\User as BaseUser;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    private $phoneNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="user", orphanRemoval=true)
     */
    private $projects;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="usersWithAccess")
     */
    private $otherProjects;



    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->roles = array('ROLE_ADMIN');
        $this->otherProjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    //This is used for the raw sql queries
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhoneNumber(): ?int
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(int $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return Collection|\App\Entity\Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setUser($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getUser() === $this) {
                $project->setUser(null);
            }
        }

        return $this;
    }

    public function getProjectsNames(): ?array
    {
        $projects = $this->getProjects();
        $names = [];
        foreach ($projects as $project){
            $names[$project->getName()] = $project->getName();
        }
        return $names;
    }

    /**
     * @return Collection|\App\Entity\Project[]
     */
    public function getOtherProjects(): Collection
    {
        return $this->otherProjects;
    }

    public function addOtherProject(Project $otherProject): self
    {
        if (!$this->otherProjects->contains($otherProject)) {
            $this->otherProjects[] = $otherProject;
        }

        return $this;
    }

    public function removeOtherProject(Project $otherProject): self
    {
        if ($this->otherProjects->contains($otherProject)) {
            $this->otherProjects->removeElement($otherProject);
        }

        return $this;
    }


}
