<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\ProjectTask;
use App\Form\AddProjectForm;
use App\Form\ChangeForm;
use App\Form\RemoveForm;
use App\Form\TaskForm;
use App\Form\SearchForm;
use App\Form\SendForm;
use App\Repository\UserRepository;
use App\Service\AccessCodeGenerator;


use App\Service\UsernameArrayGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class ProjectController extends AbstractController
{
    /**
     * @Route("/add", name="add")
     */
    public function addProject(Request $request, AccessCodeGenerator $accessCodeGenerator)
    {
        # Create a new project and set the user
        $project = new Project();
        $project->setUser($this->getUser());

        # Create the form and handle the request
        $form = $this->createForm(AddProjectForm::class, $project);
        $form->handleRequest($request);

        # The internal functionality comes here:
        if ($form->isSubmitted() && $form->isValid()) {
            $repository = $this->getDoctrine()->getRepository(Project::class);
            $repositoryUser = $this->getDoctrine()->getRepository(User::class);

            //The old version(with ORM)
            //$foundAccount = new UserAccount();
            //$foundProject = $repository->findOneBy(['user' => $this->getUser(), 'name' => $form->get('name')->getData()]);
            //$entityManager = $this->getDoctrine()->getManager();

            $foundProject = $repository->findOneByUserAndName($this->getUser(), $form->get('name')->getData());
            $users = $repositoryUser->findAllUsers();

            if ($foundProject == null) {
                $project = $form->getData();
                //$projects = $repository->findAll();
                $projects = $repository->findAllProjects($users);
                $project->setAccessCode($accessCodeGenerator->getAccessCode($projects));
                // The insertion of the project in the database ( raw sql version )
                $repository->insert($project);
                // The insertion of the project in the database using the Doctrine ORM
                //$entityManager->persist($project);
                //$entityManager->flush();
                $this->addFlash('success', 'The project was added successfully');

                return $this->redirectToRoute('addTask');
            } else {
                $this->addFlash('error', 'The project already exists');

                //throw new \RuntimeException('The project already exists');
            }
        }
        return $this->render('addProject.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/removeProject", name="removeProject")
     */
    public function removeProject(Request $request)
    {
        $projects = $this->getUser()->getProjectsNames();
        $form = $this->createForm(RemoveForm::class, ['projects' => $projects]);
        $form->handleRequest($request);
        $repository = $this->getDoctrine()->getRepository(Project::class);
        try{
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $deletedProjectsNames = $form->get('projects')->getData();
            foreach ($deletedProjectsNames as $projectName) {
                //$foundProject = $repository->findOneBy(['user' => $this->getUser(), 'name' => $projectName]);
                $foundProject = $repository->findOneByUserAndName($this->getUser(), $projectName);
                if ($foundProject != null) {
                    $repository->delete($foundProject);
                    $this->addFlash('success', 'The project was deleted');
                    //$entityManager->remove($foundProject);
                    //$entityManager->flush();
                } else {
                    $this->addFlash('error', 'The project could not be deleted');
                }
            }

        }
        }catch(\Exception $e){
            $this->addFlash('error', 'The project could not be deleted');
        }
        return $this->render('removePage.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/addTask", name="addTask")
     */
    public function addTask(Request $request)
    {
        $projects = $this->getUser()->getProjectsNames();
        $repository = $this->getDoctrine()->getRepository(Project::class);
        $taskRepository = $this->getDoctrine()->getRepository(ProjectTask::class);
        $form = $this->createForm(TaskForm::class, ['projects' => $projects]);
        $form->handleRequest($request);
        $currentUser = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            //$entityManager = $this->getDoctrine()->getManager();
            $task = new ProjectTask();
            $task->setDescription($form->get('description')->getData());
            $task->setDifficulty($form->get('difficulty')->getData());
            $task->setEstimatedTime($form->get('estimatedTime')->getData());
            $task->setPriority($form->get('priority')->getData());
            $task->setStatus($form->get('status')->getData());
            //$project = $repository->findOneBy(['user' => $currentUser, 'name' => $form->get('projectName')->getData()]);
            $project = $repository->findOneByUserAndName($currentUser, $form->get('projectName')->getData());
            if ($project != NULL) {
                $project->addTask($task);
                $taskRepository->insert($task);
                $this->addFlash('success', 'The task was added successfully');
            } else {
                $this->addFlash('error', 'Something went wrong.');
            }
            /*$entityManager->persist($task);
            $entityManager->persist($project);
            $entityManager->flush();
            //$this->redirectToRoute('addTask');*/
        }
        return $this->render('addTask.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/search", name="search")
     */
    public function searchProject(Request $request)
    {
        $form = $this->createForm(SearchForm::class);
        $form->handleRequest($request);
        $project = null;
        $searchComplete = false;
        $found = false;
        $repository = $this->getDoctrine()->getRepository(Project::class);
        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository = $this->getDoctrine()->getRepository(User::class);
            //$project = $repository->findOneBy(['accessCode' => $form->get('projectAccessCode')->getData()]);
            $project = $repository->findOneByAccessCode($userRepository, $form->get('projectAccessCode')->getData());
            if ($project == null) {
                $found = false;
            } else {
                $found = true;
            }
            $searchComplete = true;
        }

        return $this->render('searchPage.html.twig', ['form' => $form->createView(), 'project' => $project, 'found' => $found, 'searchComplete' => $searchComplete]);
    }

    /**
     * @Route("/send", name="send")
     */
    public function sendAccessCodes(Request $request, UsernameArrayGenerator $generator)
    {
        $projectRepository = $this->getDoctrine()->getRepository(Project::class);
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $projects = $this->getUser()->getProjectsNames();
        $form = $this->createForm(SendForm::class, ['projects' => $projects, 'users' => $generator->getUsers($userRepository, $this->getUser())]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sendProjectsNames = $form->get('projects')->getData();
            //$foundUser = $userRepository->findOneBy(['id' => $form->get('toUser')->getData()]);
            $foundUser = $userRepository->findOneByIdRawSQL($form->get('toUser')->getData());

            //$entityManager = $this->getDoctrine()->getManager();
            foreach ($sendProjectsNames as $projectName) {
                //$foundProject = $projectRepository->findOneBy(['name' => $projectName, 'user' => $this->getUser()]);
                $foundProject = $projectRepository->findOneByUserAndName($this->getUser(), $projectName);
                if ($foundProject != null && $foundUser != null) {
                    if ($projectRepository->findProjectFromOtherUser($foundProject, $foundUser) == false) {
                        $foundProject->addUsersWithAccess($foundUser);
                        $projectRepository->insertUserAndProject($foundProject, $foundUser);
                        $this->addFlash('success', 'The access code was sent successfully');
                    } else {
                        $this->addFlash('error', 'The user already has the code');
                    }
                    //$entityManager->persist($foundUser);
                    //$entityManager->persist($foundProject);

                }
            }
            //$entityManager->flush();
        }


        return $this->render('sendPage.html.twig', ['form' => $form->createView()]);
    }

    /*public function getUsers(): ?array
    {
        $repositoryUsers = $this->getDoctrine()->getRepository(User::class);
        $users = $repositoryUsers->findAllUsers();
        $userName = [];

        foreach ($users as $user) {
            if ($user != $this->getUser()) {
                $userName[$user->getUsername()] = $user->getId();
            }
        }

        return $userName;
    }*/

    /**
     * @Route("/change", name="change")
     */
    public function changeProject(Request $request, AccessCodeGenerator $accessCodeGenerator)
    {
        $form = $this->createForm(ChangeForm::class, ['projects' => $this->getUser()->getProjectsNames()]);
        $form->handleRequest($request);
        $repository = $this->getDoctrine()->getRepository(Project::class);
        //$entityManager = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $targetProjectName = $form->get('project')->getData();
            //$targetProject = $repository->findOneBy(['user' => $this->getUser(), 'name' => $targetProjectName]);
            $targetProject = $repository->findOneByUserAndName($this->getUser(), $targetProjectName);
            if ($targetProject != null) {
                if ($form->get('name')->getData()) {
                    $targetProject->setName($form->get('name')->getData());
                }
                $defaultDate = '2014/01/01';
                $date = date_create($defaultDate);
                if ($form->get('dueTime')->getData() != $date) {
                    $targetProject->setDueTime($form->get('dueTime')->getData());
                }
                if ($form->get('status')->getData() != $targetProject->getStatus()) {
                    $targetProject->setStatus($form->get('status')->getData());
                }
                if ($form->get('description')->getData()) {
                    $targetProject->setDescription($form->get('description')->getData());
                }
                if ($form->get('link')->getData()) {
                    $targetProject->setLink($form->get('link')->getData());
                }
                if ($form->get('workedHours')->getData()) {
                    $targetProject->setWorkedHours($form->get('workedHours')->getData());
                }
                if ($form->get('difficulty')->getData() != $targetProject->getDifficulty()) {
                    $targetProject->setDifficulty($form->get('difficulty')->getData());
                }
                if ($form->get('accessCode')->getData() == true) {
                    $userRepository = $this->getDoctrine()->getRepository(User::class);
                    $users = $userRepository->findAllUsers();
                    $allProjects = $repository->findAllProjects($users);
                    $targetProject->setAccessCode($accessCodeGenerator->getAccessCode($allProjects));
                }
                // Doctrine ORM version for update + uncomment the line 202 for this version
                //$entityManager->persist($targetProject);
                //$entityManager->flush();

                // Raw SQL version for update
                $repository->update($targetProject);
            }
        }
        return $this->render('changePage.html.twig', ['form' => $form->createView()]);
    }
}