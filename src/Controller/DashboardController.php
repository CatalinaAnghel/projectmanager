<?php
namespace App\Controller;

use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/first", name="first")
     */
    public function showIntroductionPage()
    {
        return $this->render('introductionPage.html.twig');
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function showProjects()
    {
        $projects = $this->getUser()->getProjects();
        $otherProjects = $this->getUser()->getOtherProjects();

        // redirect to a route with parameters
        return $this->render('dashboard.html.twig', ['projects' => $projects, 'otherProjects' => $otherProjects]);
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logoutAction()
    {
        # This message appears if it does not work properly
        throw new \RuntimeException('This should never be called directly.');
    }

    /**
     * @Route("/about", name="about")
     */
    public function seeAboutPage(){
        return $this->render('about.html.twig');
    }

}