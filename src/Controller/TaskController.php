<?php


namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\ProjectTask;
use App\Form\ChangeTaskForm;
use App\Form\TaskForm;
use App\Form\RemoveTaskForm;
use App\Repository\UserRepository;


use App\Service\TaskDescrArrayGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;


class TaskController extends AbstractController
{
    /**
     * @Route("/seeTasks", name="seeTasks")
     */
    public function seeTasks(Request $request)
    {
        $projects = $this->getUser()->getProjects();
        $tasks = [];
        foreach ($projects as $project){
            $tasks[$project->getName()] = $project->getTasks();
        }
        return $this->render('tasksPage.html.twig', ['tasks' => $tasks, 'projects' => $projects ]);
    }

    /**
     * @Route("/updateTask", name="updateTask")
     */
    public function updateTask(Request $request, TaskDescrArrayGenerator $taskDAGenerator)
    {

        $form = $this->createForm(ChangeTaskForm::class, [ 'tasks' => $taskDAGenerator->getTasksArray($this->getUser())]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $repository = $this->getDoctrine()->getRepository(ProjectTask::class);
            $projectRepository = $this->getDoctrine()->getRepository(Project::class);
            $userRepository = $this->getDoctrine()->getRepository(User::class);
            $foundTask = $repository->findById($form->get('task')->getData(), $projectRepository, $userRepository);
            if($foundTask)
            {
                $foundTask->setPriority($form->get('priority')->getData());
                $foundTask->setDifficulty($form->get('difficulty')->getData());
                $foundTask->setDescription($form->get('description')->getData());
                $foundTask->setEstimatedTime($form->get('estimatedTime')->getData());
                $foundTask->setStatus($form->get('status')->getData());

                $repository->update($foundTask);
                $this->addFlash('success', 'The task was updated.');
            }
            else
            {
                $this->addFlash('error', 'The task was not found.');
            }
        }
        return $this->render('changeTaskPage.html.twig', ['form' => $form->createView()]);
    }


    /**
     * @Route("/removeTask", name="removeTask")
     */
    public function removeTask(Request $request, TaskDescrArrayGenerator $taskDAGenerator)
    {
        $form = $this->createForm(RemoveTaskForm::class, ['projects' => $this->getUser()->getProjects(),
            'tasks' => $taskDAGenerator->getTasksArray($this->getUser())]);
        $form->handleRequest($request);
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $projectRepository = $this->getDoctrine()->getRepository(Project::class);
        $taskRepository = $this->getDoctrine()->getRepository(ProjectTask::class);

        if($form->isSubmitted() && $form->isValid())
        {
            $task = $taskRepository->findById($form->get('task')->getData(), $projectRepository, $userRepository);
            $taskRepository->delete($task);

        }
        return $this->render('removeTaskPage.html.twig', ['form' => $form->createView()]);

    }
}