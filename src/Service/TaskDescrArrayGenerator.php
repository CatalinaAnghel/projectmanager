<?php

namespace App\Service;

use App\Entity\ProjectTask;
use App\Entity\Project;
use App\Entity\User;

class TaskDescrArrayGenerator
{
    public function getTasksArray(User $user):?array
    {
        $projects = $user->getProjects();
        $tasks = [];
        foreach ($projects as $project)
        {
            $tasks[$project->getName()] = $this->getTasksDescriptions($project);
        }
        return $tasks;
    }

    public function getTasksDescriptions(Project $project): ?array
    {
        $tasks = $project->getTasks();

        $taskDescript = [];
        foreach ($tasks as $task)
        {
            $taskDescript[$task->getDescription()] = $task->getId();
        }
        return $taskDescript;
    }
}
