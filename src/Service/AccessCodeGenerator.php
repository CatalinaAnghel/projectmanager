<?php

namespace App\Service;

use App\Entity\User;

class AccessCodeGenerator
{
    public function getAccessCode(array $projects)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        redo:
        $accessCode = '';

        for ($i = 0; $i < 7; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $accessCode .= $characters[$index];
        }

        foreach ($projects as $project)
        {
            if($project->getAccessCode() == $accessCode){
                goto redo;
            }
        }


        return $accessCode;
    }
}
