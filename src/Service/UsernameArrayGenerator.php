<?php
namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UsernameArrayGenerator
{
    public function getUsers(UserRepository $repositoryUsers, User $currentUser): ?array
    {
        $users = $repositoryUsers->findAllUsers();
        $userName = [];

        foreach ($users as $user) {
            if ($user != $currentUser) {
                $userName[$user->getUsername()] = $user->getId();
            }
        }

        return $userName;
    }
}